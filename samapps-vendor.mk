PRODUCT_COPY_FILES += \
    vendor/samapps/Permissions/privapp-permissions-google-all.xml:product/etc/permissions/privapp-permissions-google-all.xml \
    vendor/samapps/Permissions/opengapps-default-permissions.xml:product/etc/permissions/opengapps-default-permissions.xml

PRODUCT_PACKAGES += \
    Calculator \
    GooglePhotos \
	GooglePlayMusic \
	GoogleCamera \
	GoogleMessages \
	DigitalWellbeing \
	GoogleContacts \
	GoogleDialer \
	GoogleCalendar \
	YouTube \
	GCS \
	GoogleAssistant
