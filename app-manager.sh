#!/bin/bash

###################################################
# A simple script for maintaining apps in the repo.
# Takes input for app name and apk file or URL to
# download it in this format:
# app-manager [OPTIONS] name
# Options:
# -a	add app to repo
# -r	remove file from repo
# -k	apk file to import
# -u	URL to download apk
# -p	specifies that app is priv-app
###################################################

# Exit on error, to avoid cascading errors
set -o errexit

# Get parameters passed to script and process them
while [[ "$1" =~ ^- && ! "$1" == "--" ]]; do case $1 in
  -a | --add )
    add=true
    ;;
  -r | --remove )
    remove=true
    ;;
  -k )
    shift; path=$1
    raw=true
    ;;
  -p )
    privApp=true
    ;;
  -u )
    shift; url=$1
    download=true
esac; shift; done
if [[ "$1" == '--' ]]; then shift; fi

# Get name of file to operate on and sanity check
name=$1
if [[ -z $name ]]; then
  echo "Error: must specify a name to operate on!"
  exit;
fi

# Add file to repo
if [[ $add == true ]]; then
  cd AddedGapps
  mkdir $name
  cd $name

  # Fetch file from remote or local
  if [[ $raw == true ]]; then
    mv $path AddedGapps/$name/${name}.apk
  elif [[ $download == true ]]; then
    curl $url -o ${name}.apk
  fi

  # Change to root directory
  cd ../..

  # Add necessary lines to Android.mk
  sed -i '/endif/d' Android.mk

  if [[ $privApp == true ]]; then
    echo 'include $(CLEAR_VARS)' >> Android.mk
    echo "LOCAL_MODULE := $name" >> Android.mk
    echo 'LOCAL_MODULE_OWNER := gavapps' >> Android.mk
    echo "LOCAL_SRC_FILES := AddedGapps/${name}/${name}.apk" >> Android.mk
    echo 'LOCAL_CERTIFICATE := PRESIGNED' >> Android.mk
    echo 'LOCAL_MODULE_TAGS := optional' >> Android.mk
    echo 'LOCAL_MODULE_CLASS := APPS' >> Android.mk
    echo 'LOCAL_DEX_PREOPT := false' >> Android.mk
    echo 'LOCAL_MODULE_SUFFIX := .apk' >> Android.mk
    echo 'LOCAL_PRIVILEGED_MODULE := true' >> Android.mk
    echo 'include $(BUILD_PREBUILT)' >> Android.mk
    printf "\n" >> Android.mk
  else
    # Not priv-app
    echo 'include $(CLEAR_VARS)' >> Android.mk
    echo "LOCAL_MODULE := $name" >> Android.mk
    echo 'LOCAL_MODULE_OWNER := gavapps' >> Android.mk
    echo "LOCAL_SRC_FILES := AddedGapps/${name}/${name}.apk" >> Android.mk
    echo 'LOCAL_CERTIFICATE := PRESIGNED' >> Android.mk
    echo 'LOCAL_MODULE_TAGS := optional' >> Android.mk
    echo 'LOCAL_MODULE_CLASS := APPS' >> Android.mk
    echo 'LOCAL_DEX_PREOPT := false' >> Android.mk
    echo 'LOCAL_MODULE_SUFFIX := .apk' >> Android.mk
    echo 'include $(BUILD_PREBUILT)' >> Android.mk
    printf "\n" >> Android.mk
  fi

  sed -i '$a endif' Android.mk

  # Add required lines to gavapps-vendor.mk
  sed -i '$s/$/ \\\n/' samapps-vendor.mk
  sed -i "\$a \        $name" samapps-vendor.mk
fi

# TODO: implement file remove
