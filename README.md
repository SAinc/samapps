GavApps - A small Google Apps package (w/o google play services)

I have designed this repo to be part of my nighly builds of Lineage OS 17 for my OG Google Pixel XL.

If you are wanting an easy way to build include some google apps for your marlin,
you're in the right spot, however, this package does not include ANY google play services.
If you are wanting to add Mind The GApps to your AOSP build, add
```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
        <remote name="mtg" fetch="https://gitlab.com/MindTheGapps/" />
        <project path="vendor/gapps" name="vendor_gapps" revision="qoppa" remote="mtg" />
</manifest>
``` 
to `./repo/local_manifests/mindthegapps.xml`
Once this is done, you can add this repo by adding
```
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
        <remote name="gavapps" fetch="https://gitlab.com/gt3ch1/" />
        <project path="vendor/gavapps" name="gavapps" revision="master" remote="gavapps" />
</manifest>
```
to `./repo/local_manifests/gavapps.xml`



Once you have done the above steps, you need to add the proper calls to your 
`device/$manufacturer/$product/device-$product.mk` (where $product is marlin in my case, and $manufacturer is google)
Please add the following lines to the *END* of the device-$product.mk file
```
$(call inherit-product, vendor/gapps/arm64/arm64-vendor.mk)
$(call inherit-product, vendor/gavapps/gavapps-vendor.mk)
```
Run a repo sync, and enjoy!
